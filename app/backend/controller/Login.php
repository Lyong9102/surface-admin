<?php

namespace app\backend\controller;

use think\facade\View;

use app\backend\logic\Admin as AdminLogic;

class Login extends BackendController
{

    public function index()
    {
        return View::fetch('index');
    }

    public function submit(string $username, string $password, AdminLogic $adminLogic)
    {
        $admin = $adminLogic->login($username, $password);

        if ( ! $admin)
        {
            return _error($adminLogic->getError());
        }

        session(
            'admin',
            [
                'id'       => $admin->id,
                'username' => $admin->username,
                'avatar'   => $admin->avatar ?: '/static/backend/images/avatar.png'
            ]
        );

        return _success('登录成功');
    }

}
